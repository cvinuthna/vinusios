//
//  CompleteIssueVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 05/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit
import Alamofire

class CompleteIssueVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var descBG: UIView!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    var headerText: String!
    var categoryId: String!
    
    @IBOutlet weak var tvDesc: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.text = headerText
        submit.layer.cornerRadius = 8.0
        submit.clipsToBounds = true
        
        descBG.layer.cornerRadius = 8.0
        descBG.layer.borderWidth = 0.5
        descBG.layer.borderColor = UIColor.lightGray.cgColor
        
        tvDesc.delegate = self
    }


    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if tvDesc.text == "" {
            let alert = UIAlertController(title: "Error", message: "Please enter a description", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let path = Bundle.main.path(forResource: "Info", ofType: "plist")
            let dict = NSDictionary(contentsOfFile: path!)
            let serverAddress = dict?.object(forKey: "SERVERADDRESS") as! String
            
            
            Alamofire.request("\(serverAddress)add_request_by_tenant", method: .post, parameters: ["type_id": categoryId!, "tenant_id": UserDefaults.standard.object(forKey: "id") as! String, "description": tvDesc.text!],encoding: JSONEncoding.default, headers: nil).responseString {
                response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        let json = JSON(data: response.data!)
                        if json["status"].int == 200 {
                            let alert = UIAlertController(title: "Success!", message: "Request made successfully", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                 //   print(response)
                    
                    break
                case .failure( _):
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error!", message: "There was some error", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
