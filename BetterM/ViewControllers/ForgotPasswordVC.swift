//
//  ForgotPasswordVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 08/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var btnResetPassword: UIButton!
    @IBOutlet weak var emailBG: UIView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnResetPassword.layer.cornerRadius = 8.0
        btnResetPassword.clipsToBounds = true
        
        emailBG.layer.cornerRadius = 8.0
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resetPasswordAction(_ sender: Any) {
        if tfEmail.text == "" {
            let alert = UIAlertController(title: "Email", message: "Please enter an email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            loader.startAnimating()
            let path = Bundle.main.path(forResource: "Info", ofType: "plist")
            let dict = NSDictionary(contentsOfFile: path!)
            let serverAddress = dict?.object(forKey: "SERVERADDRESS") as! String
            
            Alamofire.request("\(serverAddress)forgotPassword?email=\(tfEmail.text!)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                
                
                let json = JSON(data: response.data!)
                
                DispatchQueue.main.async {
                    self.loader.stopAnimating()
                    if json["status"].int == 200 {
                        let alert = UIAlertController(title: "Success", message: "Please check your email for your new password", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "Error", message: "Please enter a registered email", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
}
