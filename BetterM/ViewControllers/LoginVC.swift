//
//  LoginVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 03/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//


import UIKit
import Alamofire

class LoginVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var cbRememberMe: CheckBox!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var emailBG: UIView!
    @IBOutlet weak var passwordBG: UIView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogin.layer.cornerRadius = 8.0
        btnLogin.clipsToBounds = true
        
        emailBG.layer.cornerRadius = 8.0
        passwordBG.layer.cornerRadius = 8.0
        
        tfEmail.delegate = self
        tfPassword.delegate = self

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    @IBAction func loginAction(_ sender: Any) {
        performLogin()
    }
    
    func performLogin() {
        if tfEmail.text == "" {
            let alert = UIAlertController(title: "Email", message: "Please enter an email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    break
                case .cancel:
                    break
                case .destructive:
                    break
                default:
                    print("")
                }}))
            self.present(alert, animated: true, completion: nil)
        } else if tfPassword.text == "" {
            let alert = UIAlertController(title: "Password", message: "Please enter a password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    break
                case .cancel:
                    break
                case .destructive:
                    break
                default:
                    print("")
                }}))
            self.present(alert, animated: true, completion: nil)
        } else {
            
            //This code is uset to make server call to login now we will bypass it
            /*Alamofire.request("http://www.ijyaweb.com/bh_app/wp-json/custom-plugin/login?username=\(tfEmail.text!)&password=\(tfPassword.text!)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                
                let json = JSON(data: response.data!)
                
                print(json)
                DispatchQueue.main.async {
                    if json == nil {
                        let alert = UIAlertController(title: "Error", message: "Login error, Please check your credentials", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                break
                            case .cancel:
                                break
                            case .destructive:
                                break
                            default:
                                print("")
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        UserDefaults.standard.set(json["data"]["user_email"].string!, forKey: "email")
                        UserDefaults.standard.set(json["data"]["ID"].string!, forKey: "id")
                        if self.cbRememberMe.isChecked {
                            UserDefaults.standard.set("1", forKey: "is_logged_in")
                        }
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "HomeNavigationController") as! SMNavigationController
                        self.present(controller, animated: true, completion: nil)
                    }
                }
            } */
            
            
            UserDefaults.standard.set("dummy@gmail.com", forKey: "email")
            UserDefaults.standard.set("1", forKey: "id")
            if self.cbRememberMe.isChecked {
                UserDefaults.standard.set("1", forKey: "is_logged_in")
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "HomeNavigationController") as! SMNavigationController
            self.present(controller, animated: true, completion: nil)
        }
    }
}
