//
//  HomeVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 04/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: UIViewController {

    @IBOutlet weak var dealsBG: UIView!
    @IBOutlet weak var servicesBG: UIView!
    @IBOutlet weak var maintainanceBG: UIView!
    @IBOutlet weak var propertiesBG: UIView!
    var caregoryNames: [String] = []
    var categoryIds: [String] = []
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        fetchMaintainanceCategories()
        
        maintainanceBG.layer.cornerRadius = 20
        maintainanceBG.layer.shadowOpacity = 0.6
        maintainanceBG.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        propertiesBG.layer.cornerRadius = 20
        propertiesBG.layer.shadowOpacity = 0.6
        propertiesBG.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        dealsBG.layer.cornerRadius = 20
        dealsBG.layer.shadowOpacity = 0.6
        dealsBG.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        servicesBG.layer.cornerRadius = 20
        servicesBG.layer.shadowOpacity = 0.6
        servicesBG.layer.shadowOffset = CGSize(width: 0, height: 2)
    }

    @IBAction func maintainanceAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MaintanenceCategoriesVC") as! MaintanenceCategoriesVC
        
        controller.caregoryNames = caregoryNames
        controller.categoryIds = categoryIds
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func dealsAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func propertiesAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        controller.urlToOpen = "http://ijyaweb.com/bh_app"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func servicesAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        controller.urlToOpen = "http://ijyaweb.com/bh_app"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func fetchMaintainanceCategories() {
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let serverAddress = dict?.object(forKey: "SERVERADDRESS") as! String
        
        Alamofire.request("\(serverAddress)maintenance_categories", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let json = JSON(data: response.data!)
            let categories = json["categories"].array!
            
            for i in 0 ..< categories.count {
                self.caregoryNames.append(categories[i]["name"].string!)
                self.categoryIds.append(categories[i]["id"].string!)
            }
            
            DispatchQueue.main.async {
                self.loader.stopAnimating()
            }
        }

    }
}
