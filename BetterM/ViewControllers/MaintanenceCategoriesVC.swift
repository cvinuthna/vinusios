//
//  MaintanenceCategoriesVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 04/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit

class MaintanenceCategoriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return caregoryNames.count
    }
    @IBOutlet weak var searchBar: UISearchBar!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MaintainanceCell = self.tableView.dequeueReusableCell(withIdentifier: "MaintainanceCell") as! MaintainanceCell
        
        cell.name.text = caregoryNames[indexPath.row].uppercased()
        cell.button.tag = indexPath.row
        cell.button.addTarget(self, action: #selector(onItemClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    var caregoryNames: [String] = []
    var categoryIds: [String] = []
    var permanentCategoryNames: [String] = []
    var permanentCategoryIds: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        permanentCategoryNames.append(contentsOf: caregoryNames)
        permanentCategoryIds.append(contentsOf: categoryIds)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        implementSearchLogic(searchString: searchBar.text)
    }
    
    func implementSearchLogic(searchString: String!) {
        if (searchString == "") {
            caregoryNames.removeAll()
            caregoryNames.append(contentsOf: permanentCategoryNames)
            
            categoryIds.removeAll()
            categoryIds.append(contentsOf: permanentCategoryIds)
        } else {
            caregoryNames.removeAll()
            categoryIds.removeAll()
            for i in 0 ..< permanentCategoryNames.count {
                if permanentCategoryNames[i].lowercased().contains(searchString.lowercased()) {
                    caregoryNames.append(permanentCategoryNames[i])
                    categoryIds.append(permanentCategoryIds[i])
                }
            }
        }
        
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text == "") {
            caregoryNames.removeAll()
            caregoryNames.append(contentsOf: permanentCategoryNames)
            
            categoryIds.removeAll()
            categoryIds.append(contentsOf: permanentCategoryIds)
            
            tableView.reloadData()
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onItemClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
       let controller = storyboard.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        controller.headerLabelText = caregoryNames[sender.tag]
        controller.categoryId = categoryIds[sender.tag]
       
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
}
