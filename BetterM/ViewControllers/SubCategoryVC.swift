//
//  SubCategoryVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 04/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit
import Alamofire

class SubCategoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCaregoryNames.count
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MaintainanceCell = self.tableView.dequeueReusableCell(withIdentifier: "SubMaintainanceCell") as! MaintainanceCell
        
        cell.name.text = subCaregoryNames[indexPath.row].uppercased()
        cell.button.tag = indexPath.row
        cell.button.addTarget(self, action: #selector(onItemClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    var subCaregoryNames: [String] = []
    var subCategoryIds: [String] = []
    var permanentCategoryNames: [String] = []
    var permanentCategoryIds: [String] = []
    
    var categoryId: String!
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var headerLabel: UILabel!
    var headerLabelText: String = ""
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        headerLabel.text = headerLabelText
        searchBar.delegate = self
        
        maintenanceSubCategory()

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        implementSearchLogic(searchString: searchBar.text)
    }
    
    func implementSearchLogic(searchString: String!) {
        if (searchString == "") {
            subCaregoryNames.removeAll()
            subCaregoryNames.append(contentsOf: permanentCategoryNames)
            
            subCategoryIds.removeAll()
            subCategoryIds.append(contentsOf: permanentCategoryIds)
        } else {
            subCaregoryNames.removeAll()
            subCategoryIds.removeAll()
            for i in 0 ..< permanentCategoryNames.count {
                if permanentCategoryNames[i].lowercased().contains(searchString.lowercased()) {
                    subCaregoryNames.append(permanentCategoryNames[i])
                    subCategoryIds.append(permanentCategoryIds[i])
                }
            }
        }
        
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text == "") {
            subCaregoryNames.removeAll()
            subCaregoryNames.append(contentsOf: permanentCategoryNames)
            
            subCategoryIds.removeAll()
            subCategoryIds.append(contentsOf: permanentCategoryIds)
            
            tableView.reloadData()
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func maintenanceSubCategory(){
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let serverAddress = dict?.object(forKey: "SERVERADDRESS") as! String
        
        Alamofire.request("\(serverAddress)get_sub_categories?maintenance_id=\(categoryId!)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let json = JSON(data: response.data!)
            let categories = json["sub_categories"].array!
            
            for i in 0 ..< categories.count {
                self.subCaregoryNames.append(categories[i]["name"].string!)
                self.subCategoryIds.append(categories[i]["id"].string!)
            }
            DispatchQueue.main.async {
                
                self.permanentCategoryNames.append(contentsOf: self.subCaregoryNames)
                self.permanentCategoryIds.append(contentsOf: self.subCategoryIds)
                self.loader.stopAnimating()
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func onItemClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CompleteIssueVC") as! CompleteIssueVC
        controller.headerText = subCaregoryNames[sender.tag]
        controller.categoryId = subCategoryIds[sender.tag]
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
