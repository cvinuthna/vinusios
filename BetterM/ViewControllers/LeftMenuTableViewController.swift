//
//  LeftMenuTableViewController.swift
//  LNSideMenu
//
//  Created by Vinuthna Chintalapelli on 10/5/16.


import UIKit

protocol LeftMenuDelegate: class {
    func didSelectItemAtIndex(index idx: Int)
}

class LeftMenuTableViewController: UIViewController {

    weak var delegate: LeftMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //    menuTableView.reloadSections(IndexSet(integer: 0), with: .none)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    @IBAction func logoutAction(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Confirm sign out?", preferredStyle: UIAlertController.Style.alert)
        let OkAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (action:UIAlertAction) in
            UserDefaults.standard.set("0", forKey: "is_logged_in")
            let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC")
            self.present(controller, animated: true, completion: nil)
        }
        alert.addAction(OkAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

