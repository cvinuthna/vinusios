//
//  DealsVC.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 04/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class DealsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var deals: [JSON] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DealsCell = self.tableView.dequeueReusableCell(withIdentifier: "DealsCell") as! DealsCell
        cell.dealImage.kf.setImage(with: URL(string: deals[indexPath.row]["deal_image"].string ?? ""))
        cell.dealDesc.text = deals[indexPath.row]["deal_description"].string ?? ""
        cell.title.text = deals[indexPath.row]["deal_title"].string ?? ""
        cell.goBtn.tag = indexPath.row
        cell.goBtn.addTarget(self, action: #selector(onGoClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func onGoClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        controller.urlToOpen = deals[sender.tag]["deal_website"].string ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        fetchDeals()
    }
    
    func fetchDeals() {
        let path = Bundle.main.path(forResource: "Info", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let serverAddress = dict?.object(forKey: "SERVERADDRESS") as! String
        
        Alamofire.request("\(serverAddress)get_deals", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            let json = JSON(data: response.data!)
            self.deals = json["deals"].array!
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }


    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
