//
//  SMNavigationController.swift
//  LNSideMenuEffect
//
//  Created by Vinuthna Chintalapelli on 6/30/16.

import UIKit
//import LNSideMenu

class SMNavigationController: LNSideMenuNavigationController {
    
    func onListItemClicked(position: Int, type: Int) {
        
    }

    fileprivate var items:[String]?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if (UserDefaults.standard.object(forKey: "LOGINTYPE") as? String == "parent") {
//            networkManager.getStudentsForParent(user_type: "parent", userId: (UserDefaults.standard.object(forKey: "USERID") as? String)!)
//        }
        initialCustomMenu(pos: .left)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func initialSideMenu(_ position: Position) {
        menu = LNSideMenu(sourceView: view, menuPosition: position, items: items!)
        menu?.menuViewController?.menuBgColor = UIColor.black.withAlphaComponent(0.85)
        menu?.delegate = self
        menu?.underNavigationBar = true
        menu?.allowRightSwipe = false
        
        view.bringSubviewToFront(navigationBar)
    }
    
    fileprivate func initialCustomMenu(pos position: Position) {
        var vc: LeftMenuTableViewController!
        
    
            vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuTableViewController") as? LeftMenuTableViewController
        
        vc.delegate = self
        menu = LNSideMenu(navigation: self, menuPosition: position, customSideMenu: vc, size: .custom(UIScreen.main.bounds.width/1.2))//15
        menu?.delegate = self
        menu?.enableDynamic = true
        // Moving down the menu view under navigation bar
        //    menu?.underNavigationBar = true
    }
    
    fileprivate func setContentVC(_ index: Int) {
        print("Did select item at index: \(index)")
        var nViewController: UIViewController? = nil
        let userType = UserDefaults.standard.object(forKey: "LOGINTYPE") as! String
                //self.setContentViewController(nViewController!)
        // Test moving up/down the menu view
        if let sm = menu, sm.isCustomMenu {
            menu?.underNavigationBar = false
        }
    }
}

extension SMNavigationController: LNSideMenuDelegate {
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
    
    func didSelectItemAtIndex(_ index: Int) {
        setContentVC(index)
    }
}

extension SMNavigationController: LeftMenuDelegate {
    func didSelectItemAtIndex(index idx: Int) {
        menu?.toggleMenu() { [unowned self] in
            self.setContentVC(idx)
        }
    }
}

