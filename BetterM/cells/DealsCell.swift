//
//  DealsCell.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 05/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit

class DealsCell: UITableViewCell {

    @IBOutlet weak var dealDesc: UILabel!
    @IBOutlet weak var dealImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var goBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
