//
//  MaintainanceCell.swift
//  BetterM
//
//  Created by Vinuthna Chintalapelli on 04/08/19.
//  Copyright © 2019 Vinuthna Chintalapelli. All rights reserved.
//

import UIKit

class MaintainanceCell: UITableViewCell {

    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var cellBG: UIView!
    override func layoutSubviews() {
        cellBG.layer.cornerRadius = 20.0
    }
}
